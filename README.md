# Project 6: Brevet time calculator service

Name: Man Him Fung
Email: manhimf@uoregon.edu
## What is in this repository

You have a minimal implementation of Docker compose in DockerRestAPI folder, using which you can create REST API-based services (as demonstrated in class). 

## Recap 

You will reuse *your* code from project 5 (https://bitbucket.org/UOCIS322/proj5-mongo). Recall: you created the following functionalities. 1) Two buttons ("Submit") and ("Display") in the page where you have controle times. 2) On clicking the Submit button, the control times were be entered into the database. 3) On clicking the Display button, the entries from the database were be displayed in a new page. You also handled error cases appropriately. 


## Tasks

You'll turn in your credentials.ini using which we will get the following:

* The working application with three parts.

* Dockerfile

* docker-compose.yml

## How to run 

*Go to DockerRestAPI fold and run command docker-compose up

*Go to http://0.0.0.0:5002 to enter data. Then press Submit button

*To list all data in database , go to http://0.0.0.0:5000/listAll, if you want csv format go http://0.0.0.0:5000/listAll/csv

*To list open time/ close time, go to http://0.0.0.0:5000/listOpenOnly or http://0.0.0.0:5000/listCloseOnly, if you want csv format go http://0.0.0.0:5000/listOpenOnly/csv or http://0.0.0.0:5000/listCloseOnly/csv
